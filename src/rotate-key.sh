# Get the path of the wordlist:
WORDLIST_PATH="$(cd "$(dirname "$0")" && pwd)/wordlist.txt"

# Function to generate a random word from the word list:
generate_word() {
	local wordlist_line=$((RANDOM % $(wc -l <"$WORDLIST_PATH")))
	sed -n "${wordlist_line}p" "$WORDLIST_PATH"
}

selected_words=""

# Function to check if a word is already selected
is_word_selected() {
	case " $selected_words " in
	*" $1 "*) return 0 ;; # Word is already selected
	*) return 1 ;;        # Word is not selected
	esac
}

# Function to add a word to the selected words
add_to_selected() {
	selected_words="$selected_words$1 "
}

# Your main loop
for i in $(seq 1 6); do
	word=$(generate_word)

	# Check if the word is already selected
	if ! is_word_selected "$word"; then
		passphrase="$passphrase$word."
		add_to_selected "$word"
	fi
done

# echo "${passphrase::-1}"
uci set wireless.radio0_guest.key="${passphrase::-1}"
uci commit wireless
wifi up radio0
