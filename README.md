# Key Rotater

This is a simple script for OpenWRT that automatically rotates WIFI passwords at a time interval of the user's choice.

## 🌲 Configuring The Development Environment

On a system with Nix package-manager installed, `cd` into the base directory of this project and load the devshell. You can do this manually with `nix develop`, or if you have direnv installed, after `direnv allow` has been run once in the base directory, the devshell will load automatically going forwards. This will give you language server functionality and formatting for Shell Script.

## 📜 Enabling The Script

Either manually modify the crontab file or do this automatically with the justfile. It's not robust, only run it once and make sure to do it from inside `src`, lol. Then reboot the unit.
