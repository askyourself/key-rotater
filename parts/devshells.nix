{
  pkgs,
  config,
  ...
}: {
  devShells.default = pkgs.mkShell {
    packages = builtins.attrValues {
      inherit
        (pkgs)
        alejandra
        just
        shfmt
        ;
      inherit
        (pkgs.nodePackages)
        bash-language-server
        "@commitlint/config-conventional"
        ;
    };

    shellHook = "${config.pre-commit.installationScript}";
  };
}
